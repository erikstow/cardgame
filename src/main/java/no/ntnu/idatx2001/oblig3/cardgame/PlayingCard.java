package no.ntnu.idatx2001.oblig3.cardgame;

/**
 * A class representing a playing card.
 * A playing card has a suit and a face value.
 * The suit is represented by a single character, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for Clubs.
 * The face value is represented by an integer between 1 and 13.
 */
public class PlayingCard {
  
  private final char suit; 
	private final int face; 

	/**
	 * Creates an instance of a PlayingCard with a given suit and face.
	 *
	 * @param suit The suit of the card, as a single character. 'S' for Spades,
	 *             'H' for Heart, 'D' for Diamonds and 'C' for clubs
	 * @param face The face value of the card, an integer between 1 and 13
	 */
	public PlayingCard(char suit, int face) {
		this.suit = suit;
		this.face = face;
	}

	/**
	 * Returns the suit and face of the card as a string.
	 * A 4 of hearts is returned as the string "H4".
	 *
	 * @return the suit and face of the card as a string
	 */
	public String getAsString() {
		return String.format("%s%s", suit, face);
	}

	/**
	 * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for Clubs
	 *
	 * @return the suit of the card
	 */
	public char getSuit() {
		return suit;
	}
	
	/**
	 * Returns the face of the card (value between 1 and 13).
	 *
	 * @return the face of the card
	 */
	public int getFace() {
		return face;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + suit;
		result = prime * result + face;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayingCard other = (PlayingCard) obj;
		if (suit != other.suit)
			return false;
		if (face != other.face)
			return false;
		return true;
	}
}




