package no.ntnu.idatx2001.oblig3.cardgame;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Builder;

/**
 * The type Screenbuilder.
 * This class is responsible for building the GUI.
 * It implements the Builder interface from the JavaFX library.
 * The build() method returns a Region object, which is the root node of the GUI.
 * The GUI is built using JavaFX layout panes.
 */
public class Screenbuilder implements Builder<Region> {

  private DeckOfCards deck = new DeckOfCards();
  private HandOfCards hand = new HandOfCards();

  private Label handValueLabel = new Label();
  private Label heartsInHandLabel = new Label();
  private Label queenOfSpadesInHandLabel = new Label();
  private Label flushInHandLabel = new Label();

  private HBox handBox = new HBox(25);

  @Override
  public Region build() {
    BorderPane results = new BorderPane();
    results.setTop(createHeadingBox());
    results.setCenter(createHandBox());
    results.setBottom(createInfoDisplaySection());
    results.setRight(createButtonsBox());
    results.setPrefSize(1000, 1000);
    return results;
  }

  private Node createHeadingBox() {
    HBox results = new HBox();
    Text headingText = new Text("Card Game");
    headingText.setFont(Font.font("Arial", FontWeight.BOLD, 30));
    results.getChildren().add(headingText);
    results.setPadding(new Insets(6));
    results.setAlignment(Pos.CENTER);
    return results;
  }

  private Node createHandBox() {
    HBox results = new HBox(10);
    results.setPadding(new Insets(5));
    results.getChildren().add(handBox);
    results.setAlignment(Pos.CENTER);
    return results;
  }

  private Node createInfoDisplaySection() {
    HBox results = new HBox(10);
    results.setPadding(new Insets(5));
    results.getChildren().add(createGridPane());
    return results;
  }

  private Node createGridPane() {
    GridPane results = createTwoColumnGridPane();
    results.add(createInfoLabel("Hand value: "), 0, 0);
    results.add(createInfoLabel("Hearts in hand: "), 0, 1);
    results.add(createInfoLabel("Queen of Spades in hand?: "), 0, 2);
    results.add(createInfoLabel("FLUSH?! "), 0, 3);

    results.add(handValueLabel, 1, 0);
    results.add(heartsInHandLabel, 1, 1);
    results.add(queenOfSpadesInHandLabel, 1, 2);
    results.add(flushInHandLabel, 1, 3);
    return results;
  }

  private GridPane createTwoColumnGridPane() {
    GridPane results = new GridPane();
    results.getColumnConstraints().addAll(createJustifiedColumnConstraint(HPos.RIGHT),
        createJustifiedColumnConstraint(HPos.LEFT));
    results.setHgap(6);
    results.setVgap(4);
    results.setPadding(new Insets(4));
    return results;
  }

  private ColumnConstraints createJustifiedColumnConstraint(HPos alignment) {
    ColumnConstraints results = new ColumnConstraints();
    results.setHalignment(alignment);
    return results;
  }

  private Node createInfoLabel(String text) {
    Text results = new Text(text);
    results.setFont(Font.font("Arial", FontWeight.BOLD, 12));
    return results;
  }

  private Node createButtonsBox() {
    VBox results = new VBox();
    results.setAlignment(Pos.BOTTOM_RIGHT);
    results.setPadding(new Insets(8));
    results.getChildren().addAll(createDealButton(), createCheckButton());
    return results;
  }

  private Node createCheckButton() {
    Button checkButton = new Button("Check");
    checkButton.setOnAction(event -> updateHandInfo());
    checkButton.setFont(Font.font("Arial", FontWeight.BOLD, 16));
    return checkButton;
  }

  private Node createDealButton() {
    Button dealButton = new Button("Deal");
    dealButton.setOnAction(event -> updateHandBox());
    dealButton.setFont(Font.font("Arial", FontWeight.BOLD, 16));
    return dealButton;
  }

  private void updateHandInfo() {
    handValueLabel.setText(String.valueOf(hand.value()));

    heartsInHandLabel.setText(hand.getCardsOfSuit('H').stream()
        .map(PlayingCard::getAsString)
        .reduce("", (a, b) -> a + " " + b));

    if (hand.hasCard(new PlayingCard('S', 12))) {
      queenOfSpadesInHandLabel.setText("Yes!");
    } else {
      queenOfSpadesInHandLabel.setText("No!");
    }

    if (hand.hasFlush()) {
      flushInHandLabel.setText("Yes!");
    } else {
      flushInHandLabel.setText("No!");
    }
  }

  private void updateHandBox() {
    hand.setHand(deck.dealHand(5));
    handBox.getChildren().clear();
    hand.getHand().stream()
        .map(this::cardImage)
        .map(this::cardView)
        .forEach(handBox.getChildren()::add);
  }

  private ImageView cardView(Image image) {
    ImageView results = new ImageView(image);
    results.setFitHeight(100);
    results.setFitWidth(100);
    return results;
  }

  private Image cardImage(PlayingCard card) {
    return new Image(CardImagePath.find(card));
  }
}
