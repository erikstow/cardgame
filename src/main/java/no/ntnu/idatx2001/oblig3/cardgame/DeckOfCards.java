package no.ntnu.idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a standard deck of 52 playing cards, with 13 cards of each of the four suits
 * (Spades, Hearts, Diamonds, and Clubs) and 13 possible face values (Ace, 2, 3, ..., 10, Jack,
 * Queen, and King). The `DeckOfCards` class provides methods for creating and manipulating the
 * deck of cards, including shuffling the deck and dealing cards to players.
 */
public class DeckOfCards {
  
  private final char[] suit = { 'S', 'H', 'D', 'C' };
  private final int[] face = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
  private List<PlayingCard> deck;

  /**
   * Creates an instance of a DeckOfCards.
   */
  public DeckOfCards() {
    deck = new ArrayList<>();
    for (char s : suit) {
      for (int f : face) {
        deck.add(new PlayingCard(s, f));
      }
    }
  }
  
  /**
   * Returns a hand of n cards.
   * @param n number of cards to deal
   * @return a hand of n cards
   * @throws IllegalArgumentException if n is greater, or lower, than the number of cards in the deck
   */
  public List<PlayingCard> dealHand(int n) {
    
    if ((n > deck.size()) || (n < 0)) {
      throw new IllegalArgumentException("Cannot deal more, or less, cards in deck.");
    } 
    Collections.shuffle(deck);
    return deck.stream()
        .limit(n)
        .collect(Collectors.toList());
  }

  /**
   * Returns the deck of cards.
   * @return the deck of cards
   */
  public List<PlayingCard> getDeck() {
    return deck;
  }
}

