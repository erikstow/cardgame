package no.ntnu.idatx2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

/**
 * A class representing a card game.
 */
public class App extends Application {
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Region sceneRoot = new Screenbuilder().build();
        Scene scene = new Scene(sceneRoot);
        primaryStage.setTitle("Card Game");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
