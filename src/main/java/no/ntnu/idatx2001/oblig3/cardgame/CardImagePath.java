package no.ntnu.idatx2001.oblig3.cardgame;

public class CardImagePath {
  
  private CardImagePath() {
  }

  public static String find(PlayingCard card) {
    
    String path = "Playing Cards/PNG-cards-1.3/";
    
    char suit = card.getSuit(); 
    int face = card.getFace();

    String image = "";

    switch (face) {
      case 11:
        image += "jack";
        break;
      case 12:
        image += "queen";
        break;
      case 13:
        image += "king";
        break;
      case 1:
        image += "ace";
        break;
      default:
        image += face;
        break;
    }

      switch (suit) {
        case 'S':
          image += "_of_spades.png";
          break;
        case 'H':
          image += "_of_hearts.png";
          break;
        case 'D':
          image += "_of_diamonds.png";
          break;
        case 'C':
          image += "_of_clubs.png";
          break;
        default:
          break;
      }
      return path + image;
    }
}
    

