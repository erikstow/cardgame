package no.ntnu.idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class representing a hand of playing cards.
 */
public class HandOfCards {

    private List<PlayingCard> hand;
    /**
     * Creates a new HandOfCards object with an empty hand.
     */
    public HandOfCards() {
        this.hand = new ArrayList<>();
    }

    /**
     * Returns the list of PlayingCard objects representing the cards in the hand.
     *
     * @return the list of PlayingCard objects representing the cards in the hand
     */
    public List<PlayingCard> getHand() {
        return new ArrayList<>(hand);
    }

    /**
     * Sets the list of PlayingCard objects representing the cards in the hand.
     * The list must contain at least one card.
     * @param hand the list of PlayingCard objects representing the cards in the hand
     */
    public void setHand(List<PlayingCard> hand) {
        this.hand = hand;
    }
        
    /**
     * Checks if the hand contains a flush combination.
     *
     * @return true if the hand contains a flush combination, false otherwise
     */
    public boolean hasFlush() {
        if (hand.size() < 5) {
            return false;
        }
        char suit = hand.get(0).getSuit();
        return hand.stream().allMatch(card -> card.getSuit() == suit);
    }

    /**
     * Method that calculates the value of the hand.
     * @return the value of the hand.
     */
    public int value(){
        return hand.stream()
        .mapToInt(PlayingCard::getFace).
        sum();
    }

    /**
     * Method that returns cards from hand that are of a specific suit.
     * @param suit the suit of the cards to be returned.
     * @return a list of cards of the specified suit.
     * @throws IllegalArgumentException if the suit is not a valid suit.
     */
    public List<PlayingCard> getCardsOfSuit(char s)  {
        char suit = Character.toUpperCase(s);
        
        if (suit != 'S' && suit != 'H' && suit != 'D' && suit != 'C') {
            throw new IllegalArgumentException("Suit must be one of the following: S, H, D, C");
        }
        
        return hand.stream()
        .filter(card -> card.getSuit() == suit)
        .collect(Collectors.toList());
    }

    /**
     * Method that checks if the hand contains a specific card.
     * @param card the card to be checked.
     * @return true if the hand contains the card, false otherwise.
     */
    public boolean hasCard(PlayingCard card) {
       return hand.contains(card);
    }
}

