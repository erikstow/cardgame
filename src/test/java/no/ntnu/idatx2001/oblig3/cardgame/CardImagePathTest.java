package no.ntnu.idatx2001.oblig3.cardgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CardImagePathTest {

  private PlayingCard card;
   
  @BeforeEach
  void setUp() {
    card = new PlayingCard('S', 1);
  }
  
  /**
   * Test find.
   * This test method tests the find() method in the CardImagePath class.
   */
  @Test
  void testFindPositiv() {
    assertEquals("Playing Cards/PNG-cards-1.3/ace_of_spades.png", CardImagePath.find(card));
  }

  @Test
  void testFindNegative() {
    PlayingCard card2 = new PlayingCard('A', 11);
    assertNotEquals("Playing Cards/PNG-cards-1.3/jack_of_hearts.png", CardImagePath.find(card2));
  }
}
