package no.ntnu.idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

/**
 * Unit tests for the DeckOfCards class.
 */
class DeckOfCardsTest {
  DeckOfCards deck;

  /**
   * Creates an instance of a DeckOfCards.
   */
  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  /**
   * Tests the constructor.
   * Checks that the deck contains 52 cards.
   */
  @Test
  void testConstructorCardCount() {
    assertEquals(52, deck.getDeck().size());
  }

  /**
   * Tests the constructor.
   * Checks that the deck contains unique cards.
   */
  @Test
  void testConstructorUniqueCards() {
    long distinctCards = deck.getDeck().stream()
        .distinct()
        .count();
    assertEquals(52, distinctCards);
  }

  /**
   * Tests the dealHand method.
   * Checks that the method throws an IllegalArgumentException if the number of
   * cards to deal is
   * greater than the number of cards in the deck.
   */
  @Test
  void testDealHandTooManyCards() {
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
  }

  /**
   * Tests the dealHand method.
   * Checks that the method throws an IllegalArgumentException if the number of
   * cards to deal is
   * less than 0.
   */
  @Test
  void testDealHandTooFewCards() {
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(-1));
  }

  /**
   * Tests the dealHand method.
   * Checks that the method returns a hand of the correct size.
   */
  @Test
  void testDealHandCorrectSize() {
    assertEquals(5, deck.dealHand(5).size());
  }

  /**
   * Tests the dealHand method.
   * Checks that the method returns a hand of unique cards.
   */
  @Test
  void testDealHandUniqueCards() {
    long distinctCards = deck.dealHand(5).stream()
        .distinct()
        .count();
    assertEquals(5, distinctCards);
  }

  /**
   * Tests the dealHand method.
   * Checks that deck is shuffled after dealHand is called.
   */
  @Test
  void testDealHandShuffle() {
    DeckOfCards deck2 = new DeckOfCards();
    deck.dealHand(5);
    assertNotEquals(deck.getDeck(), deck2.getDeck());
  }
}
