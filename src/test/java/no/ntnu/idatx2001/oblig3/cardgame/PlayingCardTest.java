package no.ntnu.idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for PlayingCard.
 */
class PlayingCardTest {
  private char suit = 'S';
  private int face = 4;

  /**
   * Test constructor.
   */
  @Test
  void testConstructer() {
    PlayingCard card = new PlayingCard(suit, face);
    assertEquals(suit, card.getSuit());
    assertEquals(face, card.getFace());
  }

  /**
   * Test getAsString.
   */
  @Nested
  class Getters {

    @Test
    void testGetAsString() {
      PlayingCard card = new PlayingCard('S', 4);
      assertEquals("S4", card.getAsString());
    }

    /**
     * Test getSuit.
     */
    @Test
    void testGetSuit() {
      PlayingCard card = new PlayingCard('S', 4);
      assertEquals('S', card.getSuit());
    }

    /**
     * Test getFace.
     */
    @Test
    void testGetFace() {
      PlayingCard card = new PlayingCard('S', 4);
      assertEquals(4, card.getFace());
    }
  }
}
