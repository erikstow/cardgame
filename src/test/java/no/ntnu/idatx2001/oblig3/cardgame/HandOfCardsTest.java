package no.ntnu.idatx2001.oblig3.cardgame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    private HandOfCards hand = new HandOfCards();

    @BeforeEach
    void setUp() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 2));
        cards.add(new PlayingCard('S', 4));
        cards.add(new PlayingCard('S', 6));
        cards.add(new PlayingCard('S', 8));
        cards.add(new PlayingCard('S', 10));
        hand.setHand(cards);
    }

    @Test
    void testConstructor() {
        assertNotNull(hand.getHand());
        assertEquals(5, hand.getHand().size());
    }

    @Test
    void testHasFlush() {
        assertTrue(hand.hasFlush());
    }

    @Test
    void testHasFlushWithLessThanFiveCards() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 2));
        cards.add(new PlayingCard('S', 4));
        cards.add(new PlayingCard('S', 6));
        hand.setHand(cards);
        assertFalse(hand.hasFlush());
    }

    @Test
    void testHasFlushWithDifferentSuits() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 2));
        cards.add(new PlayingCard('H', 4));
        cards.add(new PlayingCard('D', 6));
        cards.add(new PlayingCard('C', 8));
        cards.add(new PlayingCard('S', 10));
        hand.setHand(cards);
        assertFalse(hand.hasFlush());
    }

    /**
     * Test for handValue method.
     */
    @Test
    void testHandValue() {
        assertEquals(30, hand.value());
    }

    /**
     * Test for getcardsofSuit method.
     */
    @Test
    void testGetCardsOfSuit() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 2));
        cards.add(new PlayingCard('H', 4));
        cards.add(new PlayingCard('C', 6));
        cards.add(new PlayingCard('S', 8));
        cards.add(new PlayingCard('H', 10));
        hand.setHand(cards);
        assertEquals(2, hand.getCardsOfSuit('H').size());
    }

    /**
     * Test for hasCard method.
     */
    @Test
    void testHasCard() {
        assertTrue(hand.hasCard(new PlayingCard('S', 2)));
    }

    /**
     * Test for setHand method.
     */
    // @Test
    // void testSetHand() {
    //     List<PlayingCard> cards = new ArrayList<>();
    //     cards.add(new PlayingCard('S', 2));
    //     cards.add(new PlayingCard('H', 4));
    //     cards.add(new PlayingCard('C', 6));
    //     cards.add(new PlayingCard('S', 8));
    //     cards.add(new PlayingCard('H', 10));
    //     cards.add(new PlayingCard('H', 13));
    //     hand.setHand(cards);
    //     assertEquals(6, hand.getHand().size());
    // }
}
